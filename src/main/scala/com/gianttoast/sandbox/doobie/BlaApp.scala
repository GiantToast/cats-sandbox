package com.gianttoast.sandbox.doobie

import cats.effect._
import doobie._
import doobie.implicits._
import doobie.util.ExecutionContexts

object BlaApp extends App {
  // Setup
  implicit val cs = IO.contextShift(ExecutionContexts.synchronous)

  val exec = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver",
    "jdbc:postgresql:catsdb",
    "catsuser",
    "catspassword",
    Blocker.liftExecutionContext(ExecutionContexts.synchronous)
  )

  case class Shelf(id: Int, name: String)
  case class Box(id: Int, name: String, shelfId: Int)
  case class Item(id: Int, name: String, boxId: Int)
  case class Thing(id: Int, name: String, shelfId: Int)

  def insertShelf(name: String): ConnectionIO[Shelf] =
    sql"INSERT INTO shelf (name) VALUES ($name)".update
      .withUniqueGeneratedKeys("id", "name")

  def insertBox(name: String, shelfId: Int): ConnectionIO[Box] =
    sql"INSERT INTO box (name, shelf_id) VALUES ($name, $shelfId)".update
      .withUniqueGeneratedKeys("id", "name", "shelf_id")

  def insertItem(name: String, boxId: Int): ConnectionIO[Item] =
    sql"INSERT INTO item (name, box_id) VALUES ($name, $boxId)".update
      .withUniqueGeneratedKeys("id", "name", "box_id")

  def insertThing(name: String, shelfId: Int): ConnectionIO[Thing] =
    sql"INSERT INTO thing (name, shelf_id) VALUES ($name, $shelfId)".update
      .withUniqueGeneratedKeys("id", "name", "shelf_id")

  // val inserts = for {
  //   shelf <- insertShelf("toast-shelf")
  //   box1  <- insertBox("foo-box", shelf.id)
  //   box2  <- insertBox("bar-box", shelf.id)
  //   _     <- insertItem("foo-item-one", box1.id)
  //   _     <- insertItem("foo-item-two", box1.id)
  //   _     <- insertItem("bar-item-one", box2.id)
  //   _     <- insertItem("bar-item-two", box2.id)
  //   _     <- insertThing("thing-one", shelf.id)
  //   _     <- insertThing("thing-two", shelf.id)
  // } yield ()

  // inserts.transact(exec).unsafeRunSync

  // Programs
  val p = sql"""
    SELECT
        s.id,
        s.name as shelf_name,
        b.id as b_id,
        b.name as b_name,
        b.shelf_id,
        i.id as i_id,
        i.name as i_name,
        i.box_id,
        t.id as t_id,
        t.name as t_name,
        t.shelf_id as t_shelf_id
    FROM shelf s
    INNER JOIN box b ON s.id = b.shelf_id
    INNER JOIN item i ON b.id = i.box_id
    INNER JOIN thing t ON s.id = t.shelf_id
  """.query[(Shelf, Box, Item, Thing)].to[List]

  val result = p.transact(exec).unsafeRunSync

  println(result)
}
