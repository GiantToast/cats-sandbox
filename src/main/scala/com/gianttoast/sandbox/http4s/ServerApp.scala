package com.gianttoast.sandbox.http4s

import cats.effect._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server._
import org.http4s.server.blaze._
import org.http4s.client._
import org.http4s.client.blaze._
import scala.concurrent.ExecutionContext.global

object Server extends IOApp {
  // Routes, Grouped into Services
  val helloWorldService: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / "hello" / name => Ok(s"Hello, $name")
  }

  val toastService = HttpRoutes.of[IO] {
    case GET -> Root / "toast" => BadRequest("Something Bad With Your Shite")
    case GET -> Root / "ok"    => Ok(())
  }

  // App
  val httpApp = Router("/" -> helloWorldService, "/api" -> toastService).orNotFound

  // Server
  val server: Resource[IO, Server[IO]] = BlazeServerBuilder[IO]
    .bindHttp(8080, "localhost")
    .withHttpApp(httpApp)
    .resource

  // Client
  val client: Resource[IO, Client[IO]] = BlazeClientBuilder[IO](global).resource

  def run(args: List[String]): IO[ExitCode] = {
    // Run the Server in the Background
    val fiber: Fiber[IO, Nothing] = server.use(_ => IO.never).start.unsafeRunSync()

    // IOs For Client Requests
    val hello: IO[String] = client.use { _.expect[String]("http://localhost:8080/hello/toast") }
    val toast: IO[String] = client.use { _.expect[String]("http://localhost:8080/api/toast") }
    val ok: IO[Unit]      = client.use { _.expect[Unit]("http://localhost:8080/api/ok") }

    // Run the IOs
    val result1 = hello.attempt.unsafeRunSync()
    val result2 = toast.attempt.unsafeRunSync()
    val result3 = ok.attempt.unsafeRunSync()

    // Print the Results
    println(result1)
    println(result2)
    println(result3)

    // Tear Down the Server
    fiber.cancel.map(_ => ExitCode.Success)
  }
}
