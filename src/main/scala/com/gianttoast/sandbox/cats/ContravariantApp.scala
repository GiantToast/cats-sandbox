package com.gianttoast.sandbox.cats

import cats.Contravariant

object ContravariantApp extends App {
  // Contravariant
  // A contraviant represents a type constrcutor that can be "contramap"ed. To contramap over a type is to get
  // a new instance of that type by prepending a transformation.
  // E.g. given a F[B] and a function A => B, contramap will give you an F[A].


}
