package com.gianttoast.sandbox.cats

import cats.Functor
import cats.instances.option._
import cats.instances.list._
// import cats.syntax.functor._

object FunctorApp extends App {
  // Functor
  // A functor represents a type constructor that can be "map"ed over. To map over a type is to apply a given
  // function to every element of the type constructors inner type to produce a new instance of the type
  // constructor whose inner type is that of what the function returns.

  // "map" example
  // "map" on a List[Int] accepts a function of Int => A (String in this case) and returns a List[A] (List[String])
  // List is a type constructor and can be considered a Functor[List[_]]
  val xs: List[Int]         = List(1, 2, 3)
  val result1: List[String] = xs.map(x => s"Bla: $x")
  println(s"Map Example Result1: $result1")

  // Functor instances must obey two laws
  // Mapping with a function f and again with a function g, is the same as mapping with the composition of f and g
  // E.g. xs.map(f).map(g) == xs.map(x => g(f(x)))
  // Mapping with the identity function is the same as doing nothing
  // E.g. xs.map(x => x) == xs

  // Laws Example
  // Composition
  val optFunctor                          = Functor[Option]
  def intToString(x: Int): String         = s"Stringified: $x"
  def stringToBoolean(x: String): Boolean = x.isEmpty
  val option                              = Some(23)
  val firstMapResult                      = optFunctor.map(option)(intToString)
  val secondMapResult                     = optFunctor.map(firstMapResult)(stringToBoolean)
  val composedResult                      = optFunctor.map(option)(x => stringToBoolean(intToString(x)))

  println(s"Functor map f map g: $secondMapResult")
  println(s"Functor map g(f): $composedResult")
  println(s"Functor Compostion Law: ${secondMapResult == composedResult}")

  // Identity
  val identityResult = optFunctor.map(option)(x => x)
  println(s"Functor map identity: $identityResult")
  println(s"Functor Identity Law: ${option == identityResult}")

  // Functors also have a lift method that uses the Functor instance's "map" method to turn a
  // function A => B into a function F[A] => F[B]
  // Example
  // Turn our Int => String function "intToString" into an Option[Int] => Option[String]
  val optionIntToString: Option[Int] => Option[String] = optFunctor.lift(intToString)
  println(s"Lifted function result: ${optionIntToString(option)}")

  // Functors can be composed.
  // If A and B have a functor instance, then so does F[G[_]]
  // You can create these new instances with the "compose" method on an instance of functor
  val listOptFunctor = Functor[List].compose[Option]
  val xs2            = List(Some(1), None, Some(2))
  println(s"Composed functor input: $xs2")
  println(s"Composed functor result: ${listOptFunctor.map(xs2)(_ + 1)}")
}
