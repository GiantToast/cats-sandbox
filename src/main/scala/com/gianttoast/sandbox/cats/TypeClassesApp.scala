package com.gianttoast.sandbox.cats

object TypeClassesApp extends App {
  // Type Classes
  // Tool used in functional programming to acheive ad-hoc polymorphism

  // For example:
  def combineInts(xs: List[Int]): Int          = xs.foldLeft(0)(_ + _)
  def combineStrings(xs: List[String]): String = xs.foldLeft("")(_ + _)
  def combineSets[A](xs: List[Set[A]]): Set[A] = xs.foldLeft(Set.empty[A])(_ union _)

  val ints = List(1, 2, 3)
  println(s"Combine Ints: ${combineInts(ints)}")

  val strings = List("I ", "am ", "Batman!")
  println(s"Combine Strings: ${combineStrings(strings)}")

  val sets = List(Set(1), Set(2), Set(3))
  println(s"Combine Sets: ${combineSets(sets)}")

  // These all perform the same type of operation
  // They need an empty initial state, and a wide to combine
  // We can write a type class that tells us what "empty" and "combine" mean for different types

  trait Combinable[A] {
    def empty: A
    def combine(x: A, y: A): A
  }

  // Well that was easy
  // This is our TypeClass, it wraps a Type and defines some abstract methods
  // Next we need some concrete, implicit instances for types we want to combine

  object Combinable {
    def apply[A](implicit ev: Combinable[A]): Combinable[A] = ev

    implicit val combinableInt: Combinable[Int] = new Combinable[Int] {
      def empty: Int                   = 0
      def combine(x: Int, y: Int): Int = x + y
    }

    implicit val combinableString: Combinable[String] = new Combinable[String] {
      def empty: String                         = ""
      def combine(x: String, y: String): String = x + y
    }

    // Needs to be def so we capture the generic Set type A
    implicit def combinableSet[A]: Combinable[Set[A]] = new Combinable[Set[A]] {
      def empty: Set[A]                         = Set.empty[A]
      def combine(x: Set[A], y: Set[A]): Set[A] = x union y
    }
  }

  // Now we can write a function that abstracts over any type,
  // as long as it has an implicit combinable in scope

  def combineStuff[A](xs: List[A])(implicit ev: Combinable[A]): A =
    xs.foldLeft(ev.empty)(ev.combine)

  println(s"CombineStuff Ints: ${combineStuff(ints)}")
  println(s"CombineStuff Strings: ${combineStuff(strings)}")
  println(s"CombineStuff Sets: ${combineStuff(sets)}")

  // There is some syntatic sugar for implicit arguments
  // Without the apply on the Combinable object, we would need to use "implicitly" in the body
  def combineStuffAgain[A: Combinable](xs: List[A]): A =
    xs.foldLeft(Combinable[A].empty)(Combinable[A].combine)

  println(s"CombineStuffAgain Ints: ${combineStuffAgain(ints)}")
  println(s"CombineStuffAgain Strings: ${combineStuffAgain(strings)}")
  println(s"CombineStuffAgain Sets: ${combineStuffAgain(sets)}")

  // This is ad-hoc polymorphism, because our Ints, Strings, etc are being changed
  // into Combinable[Int]s,  Combinable[String]s, etc on demand in an ad-hoc way
  // Now we only need to define new instances of Combinable to make any function defined
  // against it's interface work over our type

  // There are lots of other thing implicits can do.
  // Look up implicit conversion and scope for more info.
}
