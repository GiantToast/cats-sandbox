package com.gianttoast.sandbox.cats

import cats.Semigroup
import cats.instances.int._
import cats.instances.map._
import cats.syntax.semigroup._

object SemigroupApp extends App {
  // Semigroup
  // A semigroup represents a type that can be combined to produce new values of that type
  // A type can be a semigroup as long as they can be combined in an associative way
  // E.g. x + (y + z) == (x + y) + z

  // As long as this law holds for any value that can be chosen from that type, it can be a semigroup

  // Simple Example
  // An illustrative example is Ints
  val si      = Semigroup[Int]
  val result1 = si.combine(1, si.combine(2, 3))
  val result2 = si.combine(si.combine(1, 2), 3)

  println(s"Int Semigroup Result1: $result1")
  println(s"Int Semigroup Result2: $result2")
  println(s"Int Associative Law: ${result1 == result2}")

  // More Exciting Example
  // Semigroups can exist for much more complex types
  val sm      = Semigroup[Map[String, Int]]
  val m1      = Map("I am" -> 1, "an orphan" -> 3)
  val m2      = Map("I am" -> 2, "Batman" -> 4)
  val result3 = sm.combine(m1, m2)

  println(s"Map Semigroup Result: $result3")

  // In this case the values for each key are also combined via a semigroup.
  // this means you need to have an implicit instance for Semigroup[Int] in scope.
  // The following line will not compile:
  // val sm2 = Semigroup[Map[String, Double]]

  // Syntax
  // Semigroup offers extension methods so you don't have to create an instance directly
  val result4 = 1 |+| (2 |+| 3)
  val result5 = (1 |+| 2) |+| 3

  println(s"Int Semigroup Result1: $result3")
  println(s"Int Semigroup Result2: $result4")
  println(s"Int Associative Law: ${result3 == result4}")
}
