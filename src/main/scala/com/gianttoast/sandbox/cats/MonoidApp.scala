package com.gianttoast.sandbox.cats

import cats.Monoid
import cats.instances.int._
import cats.data.NonEmptyList
// import cats.syntax.monoid._

object MonoidApp extends App {
  // Monoid
  // Monoid is just a semigroup with an empty value that acts as the identity for the combine method
  // E.g. x combine empty == x

  // As long as this law holds for any value of x for a given type, that type can be a monoid

  // Simple Example
  // Ints can be a monoid. The example from SemigroupApp works the same way.
  val mi = Monoid[Int]
  val result1 = mi.combine(1, mi.combine(2, 3))
  val result2 = mi.combine(mi.combine(1, 2), 3)

  println(s"Int Monoid Result1: $result1")
  println(s"Int Monoid Result2: $result2")
  println(s"Int Associative Law: ${result1 == result2}")

  // Additonally, we have an identity element
  val result3 = mi.combine(1, mi.empty)
  println(s"Int Identity Result: $result3")
  println(s"Int Identity Law: ${1 == result3}")

  // Having an identity element allows us to write functions that we couldn't with Semigroups
  // For example, defining a general collapse function that specificies how to fold over values
  def collapse[A: Monoid](xs: List[A]): A = xs.foldLeft(Monoid[A].empty)(Monoid[A].combine)

  val result4 = collapse(List(1, 2, 3))

  println(s"Int Collapse Result: $result4")

  // This collapse method now works for any type that has an implicit Monoid in scope
  import cats.instances.string._

  val result5 = collapse(List("I am ", "Batman", ", an orphan"))

  println(s"String Collapse Result: $result5")

  // Better Example
  // Some types are semigroups but not monoids
  // Cats has a non empty list type, it combines like normal lists
  // but because it is non empty, it has no empty element and therefore can't be a monoid
  val nel1 = NonEmptyList(head = 1, tail = List(2, 3))
  val result6 = nel1 ++ List(4, 5, 6)

  println(s"NonEmptyList result: $result6")

  // Syntax
  // Monoids include the semigroup syntax
}
