# Functional Libraries in Scala

## Cats
### Provided TypeClasses
Semigroup
Monoid
Applicative
Traversable
Functor
Traverse
Monad
Comonad
Contravariant
ContravariantMonoidal
Invariant
InvariantMonoidal
Alternative
Bifunctor
Eq
Foldable
Parallel
SemigroupK
MonoidK
Show
Reducible
NonEmptyTraverse
Arrow

### Data Types
Chain
Const
ContT
Either
Eval
FreeApplicatives
FreeMondads
FunctionK
Id
Ior
Kleisli
Nested
NonEmptyList
OneAnd
OptionT
EitherT
IorT
State
Validated

## Cats Effect
### Provided TypeClasses
Bracket
Sync
LiftIO
Async
Concurrent
Effect
ConcurrentEffect

### Data Types
IO
SyncIO
Fiber
Resource
Clock
ContextShift
Timer
IOApp
Deferred
MVar
Ref
Semaphore
