name := "cats-sandbox"
version := "0.0.1-SNAPSHOT"

scalaVersion := "2.12.10"

scalacOptions ++= Seq(
  "-encoding",
  "UTF-8",
  "-deprecation",
  "-unchecked",
  "-feature",
  "-language:higherKinds",
  "-Xlint",
  "-Xfatal-warnings",
  "-Ypartial-unification"
)

libraryDependencies ++= Seq(
  "org.typelevel"     %% "cats-core"           % "2.0.0",
  "org.typelevel"     %% "cats-effect"         % "2.0.0",
  "com.chuusai"       %% "shapeless"           % "2.3.3",
  "co.fs2"            %% "fs2-core"            % "2.0.1",
  "co.fs2"            %% "fs2-io"              % "2.0.1",
  "is.cir"            %% "ciris-core"          % "0.13.0-RC1",
  "is.cir"            %% "ciris-cats"          % "0.13.0-RC1",
  "is.cir"            %% "ciris-cats-effect"   % "0.13.0-RC1",
  "is.cir"            %% "ciris-enumeratum"    % "0.13.0-RC1",
  "is.cir"            %% "ciris-generic"       % "0.13.0-RC1",
  "is.cir"            %% "ciris-refined"       % "0.13.0-RC1",
  "io.circe"          %% "circe-core"          % "0.11.1",
  "io.circe"          %% "circe-generic"       % "0.11.1",
  "io.circe"          %% "circe-parser"        % "0.11.1",
  "org.http4s"        %% "http4s-dsl"          % "0.20.11",
  "org.http4s"        %% "http4s-blaze-server" % "0.20.11",
  "org.http4s"        %% "http4s-blaze-client" % "0.20.11",
  "io.chrisdavenport" %% "linebacker"          % "0.2.1",
  "org.tpolecat"      %% "doobie-core"         % "0.8.4",
  "org.tpolecat"      %% "doobie-hikari"       % "0.8.4",
  "org.tpolecat"      %% "doobie-postgres"     % "0.8.4",
  "org.scalactic"     %% "scalactic"           % "3.0.8",
  // Tests
  "org.scalatest" %% "scalatest"        % "3.0.8" % "test",
  "org.typelevel" %% "cats-effect-laws" % "2.0.0" % "test",
  "org.tpolecat"  %% "doobie-scalatest" % "0.8.4" % "test"
)

resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.3")
